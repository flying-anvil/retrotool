<?php

use FlyingAnvil\RetroTool\Converter\Image\Snes\GfxTilesetParser;
use FlyingAnvil\Libfa\DataObject\Color\ColorPalette\IndexedColorPalettes;
use FlyingAnvil\Libfa\Image\Drawing\Tile\TileMap;
use FlyingAnvil\Libfa\Wrapper\File;

require_once __DIR__ . '/../vendor/autoload.php';

$gfx = File::load(__DIR__ . '/../samples/ExGFX081.bin');
$pal = File::load(__DIR__ . '/../samples/swamp.pal');
$paletteIndex = 2;

//$gfx = File::load(__DIR__ . '/../samples/rad.bin');
//$pal = File::load(__DIR__ . '/../samples/rad.pal');
//$paletteIndex = 0;

//$paletteIndex = random_int(0, 7);

$palette = IndexedColorPalettes::loadFromPalFile($pal, 16);

$converter = new GfxTilesetParser();
$tiles     = $converter->convert($gfx);
$tileMap   = TileMap::createFromTiles(
    $tiles,
    16, 16,
    8, 8,
);

$tileMap->drawSmall($palette->getPaletteByIndex($paletteIndex), true);

echo PHP_EOL;
